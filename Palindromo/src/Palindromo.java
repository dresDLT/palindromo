import java.util.Scanner;

public class Palindromo {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		Scanner sc = new Scanner(System.in);
		
		System.out.print("INGRESAR FRASE: ");
		//String frase = "anita lava la tina";	
		String frase = sc.nextLine();

		System.out.println("\nEs Palindromo? --> " + esPalindromo(frase));
		System.out.println("\tfrase: "+frase.replace(" ", ""));
		System.out.println("\tv_frase: "+voltear(frase.replace(" ", "")));
		
		
	}
	
	public static String voltear(String str) {
		if ((str.length() <= 1)) {
			return str;
		}
		return voltear(str.substring(1)) + str.charAt(0);
	}
	
	public static String esPalindromo(String frase){
		if (frase.replace(" ", "").equalsIgnoreCase(voltear(frase.replace(" ", "")))){
			return "SI";
		} else{
			return "NO";
		}
	}

}
